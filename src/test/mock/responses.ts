import {PlayerScore, Quiz, QuizShort} from "../../utils/types.tsx";

export const PLAYER_SCORES: PlayerScore[] = [
    {
        id: "123",
        score: 1010,
        playerName: "Player1",
    },
    {
        id: "234",
        score: 2020,
        playerName: "Player2",
    },
    {
        id: "345",
        score: 3030,
        playerName: "Player3",
    },
    {
        id: "456",
        score: 800,
        playerName: "GulaschGönner",
    }
]
export const QUIZZES: QuizShort[] = [
    {
        id: "1",
        name: "Allgemeinwissen",
        description: "Ein Quiz, um dein Allgemeinwissen zu testen.",
        questionCount: 3
    },
    {
        id: "2",
        name: "Mathematik",
        description: "Teste dein Wissen in Mathematik.",
        questionCount: 3
    },
];

export const QUIZ1: Quiz = {
    id: "1",
    name: "Allgemeinwissen",
    description: "Ein Quiz, um dein Allgemeinwissen zu testen.",
    scores: PLAYER_SCORES,
    questions: [
        {
            id: "1-1",
            text: "Welches ist das größte Land der Welt nach Fläche?",
            answers: [
                {id: "1-1-1", text: "China", istrue: false},
                {id: "1-1-2", text: "USA", istrue: false},
                {id: "1-1-3", text: "Russland", istrue: true},
                {id: "1-1-4", text: "Kanada", istrue: false},
            ],
        },
        {
            id: "1-2",
            text: "Wie viele Planeten gibt es im Sonnensystem?",
            answers: [
                {id: "1-2-1", text: "7", istrue: false},
                {id: "1-2-2", text: "8", istrue: true},
                {id: "1-2-3", text: "9", istrue: false},
                {id: "1-2-4", text: "10", istrue: false},
            ],
        },
        {
            id: "1-3",
            text: "Wer schrieb 'Faust'?",
            answers: [
                {id: "1-3-1", text: "Friedrich Schiller", istrue: false},
                {id: "1-3-2", text: "Gotthold Ephraim Lessing", istrue: false},
                {id: "1-3-3", text: "Johann Wolfgang von Goethe", istrue: true},
                {id: "1-3-4", text: "Heinrich Heine", istrue: false},
            ],
        },
    ],
}

export const QUIZ2: Quiz = {
    id: "2",
    name: "Mathematik",
    description: "Teste dein Wissen in Mathematik.",
    scores: PLAYER_SCORES,
    questions: [
        {
            id: "2-1",
            text: "Was ist 5 + 7?",
            answers: [
                {id: "2-1-1", text: "10", istrue: false},
                {id: "2-1-2", text: "11", istrue: false},
                {id: "2-1-3", text: "12", istrue: true},
                {id: "2-1-4", text: "13", istrue: false},
            ],
        },
        {
            id: "2-2",
            text: "Wie lautet die Quadratwurzel von 64?",
            answers: [
                {id: "2-2-1", text: "6", istrue: false},
                {id: "2-2-2", text: "7", istrue: false},
                {id: "2-2-3", text: "8", istrue: true},
                {id: "2-2-4", text: "9", istrue: false},
            ],
        },
        {
            id: "2-3",
            text: "Wie viele Seiten hat ein Hexagon?",
            answers: [
                {id: "2-3-1", text: "5", istrue: false},
                {id: "2-3-2", text: "6", istrue: true},
                {id: "2-3-3", text: "7", istrue: false},
                {id: "2-3-4", text: "8", istrue: false},
            ],
        },
    ],
}