import { expect, test } from 'vitest'
import {render, screen} from "@testing-library/react";
import App from "../../App.tsx";


test('Rendert Loginscreen', () => {
    render(<App/>);
    const linkElement = screen.getByText("Anmelden");
    expect(linkElement).toBeInTheDocument();
});