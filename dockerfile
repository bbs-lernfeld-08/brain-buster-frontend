FROM node:20.14-alpine
WORKDIR /app
COPY package*.json .
RUN npm install
COPY . .
RUN npm run build
RUN npm prune --production
EXPOSE 3030 3030
WORKDIR /app/dist
CMD ["npm", "run", "serve"]